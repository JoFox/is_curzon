import unittest
from is_curzon import is_curzon


class Test_Is_Curzon(unittest.TestCase):
    def test(self):
        result1 = is_curzon(5)
        result2 = is_curzon(10)
        result3 = is_curzon(14)

        self.assertEqual(result1, True)
        self.assertEqual(result2, False)
        self.assertEqual(result3, True)


if __name__ == '__main__':
    unittest.main()

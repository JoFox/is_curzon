#!/bin/bash

pip install flake8

flake8 . --extend-exclude=dist,build,__pycache__ --show-source --statistics

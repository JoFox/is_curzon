def is_curzon(num):
    power = int(2 ** num + 1)
    multi = int(2 * num + 1)
    x = power % multi
    if x == 0:
        return True
    else:
        return False
